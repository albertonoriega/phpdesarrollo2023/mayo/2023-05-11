<?php
require_once "conexion.inc";

$conexion = conectar('desarrollo');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <?php
    require_once "_menu.php";
    ?>
    <div class="contenido">
        <p>Pincha en alumnos para ver el contenido de todos los alumnos</p>
        <p>Pincha en exámenes para ver el contenido de todos los exámenes</p>
        <img src="imgs/examen2.jpg" alt="">
    </div>
</body>

</html>