<?php
require_once "conexion.inc";

$conexion = conectar('desarrollo');

$todo = alumnosYExamenes($conexion);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <?php
    require_once "_menu.php";
    require_once "_todo.php";
    ?>

</body>

</html>