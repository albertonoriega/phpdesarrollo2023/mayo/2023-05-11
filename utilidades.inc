<?php
function obtenerPagina()
{
    // Comprobamos en que página estamos para mostrar activo el li correspondiente del menu 

    // Constantes de servidor
    // __FILE__ nombre del archivo php con la ruta
    //var_dump(__FILE__); //ruta absoluta (desde la raiz del ordenador) y nombre
    // 'C:\laragon\www\Desarrollo2023\php\ejemplos\mayo\2023-05-11\_menu.php'

    // OTRA FORMA

    // variables del servidor
    // $_SERVER es un array con datos del servidor
    // var_dump($_SERVER['PHP_SELF']); // ruta relativa (desde la raiz del proyecto) y nombre
    // '/Desarrollo2023/php/ejemplos/mayo/2023-05-11/index.php'

    // COMO SABER EN QUE PÁGINA ESTAMOS

    // Nos quedamos con la ruta que viene después de la última /
    // $pagina = (strrchr($_SERVER['PHP_SELF'], '/'));

    // OTRA FORMA 
    // Explode crea una array de strings utilizando un carácter
    $a = explode('/', $_SERVER['PHP_SELF']);
    // array_pop te quita el ultimo elemento del array
    $pagina = array_pop($a);
    return $pagina;
}

function dibujarMenu($pagina, $menu)
{
?>
    <ul>
        <li> <img src="imgs/examen.png" width="40px" alt=""></li>
        <?php
        // Crear el menu
        foreach ($menu as $etiqueta => $href) {
            if ($href == $pagina) {
                echo "<li ><a class= 'activo' href=\"{$href}\">{$etiqueta}</a></li>";
            } else {
                echo "<li><a href=\"{$href}\">{$etiqueta}</a></li>";
            }
        }

        ?>
    </ul>
<?php
}
