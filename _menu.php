<?php
require_once 'utilidades.inc';

$pagina = obtenerPagina();


$menu = [
    'Inicio' => 'index.php',
    'Alumnos' => 'alumnos.php',
    'Exámenes' => 'examenes.php',
    'Todo' => 'todo.php',
];

// para que se apliquen los estilos, el menu tiene que estar dentro de un ul
echo '<nav>';
dibujarMenu($pagina, $menu);
echo '</nav>';
