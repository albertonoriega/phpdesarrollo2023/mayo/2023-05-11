<?php
// Subvista que muestra todos los datos de las dos tablas
// Necesito un array llamado todo array $todo
?>

<div class="contenidoTablas">
    <h2>Todos los datos</h2>
    <table style="text-align: center;">
        <thead style="background-color: #ccc;">
            <tr>
                <td>Código</td>
                <td>Nombre</td>
                <td>Correo</td>
                <td>Titulo</td>
                <td>Nota</td>
                <td>Fecha</td>

            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 0; $i < count($todo); $i++) {
            ?>
                <tr>
                    <td> <?= $todo[$i]["codigo"] ?></td>
                    <td> <?= $todo[$i]["nombre"] ?></td>
                    <td> <?= $todo[$i]["correo"] ?></td>
                    <td> <?= $todo[$i]["titulo"] ?></td>
                    <td> <?= $todo[$i]["nota"] ?></td>
                    <td> <?= $todo[$i]["fecha"] ?></td>


                </tr>
            <?php
            }
            ?>
</div>