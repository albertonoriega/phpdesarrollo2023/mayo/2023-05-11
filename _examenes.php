<?php
// Subvista que muestra todos los examenes
// Necesito un array llamado examenes array $examenes
?>
<div class="contenidoTablas">
    <h2>Listado de exámenes</h2>
    <table style="text-align: center;">
        <thead style="background-color: #ccc;">
            <tr>
                <td>ID</td>
                <td>Titulo</td>
                <td>Nota</td>
                <td>Fecha</td>
                <td>Código Alumno</td>
            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 0; $i < count($examenes); $i++) {
            ?>
                <tr>
                    <td> <?= $examenes[$i]["id"] ?></td>
                    <td> <?= $examenes[$i]["titulo"] ?></td>
                    <td> <?= $examenes[$i]["nota"] ?></td>
                    <td> <?= $examenes[$i]["fecha"] ?></td>
                    <td> <?= $examenes[$i]["codigoAlumno"] ?></td>

                </tr>
            <?php
            }
            ?>
</div>