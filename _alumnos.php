<?php
// Subvista que muestra todos los alumnos
// Necesito un array llamado alumnos array $alumnos
?>
<div class="contenidoTablas">
    <h2>Listado de alumnos</h2>
    <table style="text-align: center;">
        <thead style="background-color: #ccc;">
            <tr>
                <td>Código</td>
                <td>Nombre</td>
                <td>Correo</td>
            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 0; $i < count($alumnos); $i++) {
            ?>
                <tr>
                    <td> <?= $alumnos[$i]["codigo"] ?></td>
                    <td> <?= $alumnos[$i]["nombre"] ?></td>
                    <td> <?= $alumnos[$i]["correo"] ?></td>
                </tr>
            <?php
            }
            ?>

        </tbody>
    </table>
</div>