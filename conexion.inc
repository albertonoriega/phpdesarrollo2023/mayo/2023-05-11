<?php


/**
 * Función que nos permite conectar a una base de datos 
 *
 * @param  string $baseDeDatos nombre de la base de datos a la que te vas a conectar
 * @return object Conexión a la base de datos
 */
function conectar($baseDeDatos)
{
    $conexion = new mysqli(
        '127.0.0.1',  // servidor de base de datos
        'root', // usuario
        '', // contraseña
        $baseDeDatos, // nombre de la base de datos
        3306, //puerto
    );
    return $conexion;
}


/**
 * Me permite ejecutar cualquier consulta de sql
 *
 * @param  object $conexion Conexión a la base de datos
 * @param  string $consulta Consulta de SQL a ejecutar
 * @return object objeto de tipo mysqli_result 
 */
function consulta($conexion, $consulta)
{
    return $conexion->query($consulta);
}

/**
 * Nos devuelve un array con todos los registros de la tabla alumnos
 *
 * @param  object $conexion Conexión a la base de datos
 * @return array Array con todos los alumnos
 */
function alumnos($conexion)
{
    $ConsultaAlumnos = $conexion->query("select * from alumnos");
    return $ConsultaAlumnos->fetch_all(MYSQLI_ASSOC);
}

/**
 * Nos devuelve un array con todos los registros de la tabla examenes
 *
 * @param  mixed $conexion Conexión a la base de datos
 * @return array Array con todos los examenes
 */
function examenes($conexion)
{
    $ConsultaExamenes = $conexion->query("select * from examenes");
    return $ConsultaExamenes->fetch_all(MYSQLI_ASSOC);
}

/**
 * Función que te devuelve los registros de las dos tablas
 *
 * @param  object $conexion Conexión a la base de datos
 * @return array $ConsultaAlumnosYExamenes Array con todos los registros de las dos tablas
 */
function alumnosYExamenes($conexion)
{
    $ConsultaAlumnosYExamenes = $conexion->query("SELECT * FROM alumnos a INNER JOIN examenes e ON a.codigo = e.codigoAlumno");
    return $ConsultaAlumnosYExamenes->fetch_all(MYSQLI_ASSOC);
}
